﻿# <center>ACEPAY 商户代付开发手册</center> #

<center>版本 1.0</center>


## 1. 名词解释

&emsp;&emsp;用户：是指在商户平台使用的客户。


&emsp;&emsp;商户：向支付平台发送充值提现申请需求。


&emsp;&emsp;支付平台：ACEPAY支付。


&emsp;&emsp;代付密钥：ACEPAY支付公司提供的用于代付的密码。


----------


## 2. 接口标准
&emsp;&emsp;ACEPAY支付平台，使用HTTP协议进行数据通信，请求数据以POST形式提交，请求的`Content-Type`为`application/x-www-form-urlencoded`。响应方式是JSON格式。所有的请求与响应都使用UTF-8字符集进行编码。

### 2.1. 接口介绍

**代付请求**

&emsp;&emsp;请求URL：https://api.acepay.net/api/v2/daifu/request

&emsp;&emsp;请求示例：`curl -H "Content-Type: application/x-www-form-urlencoded" -X POST -d "account_type=bank_card&amount=100&bank_code=78&comment=comment&company_id=aaaccc&currency=CNY&enc_data=4QiS4CnswETuJPqK0NghxGs%2Bwo4rhy3PAO597pXRfucR6TH93A8B05J7v4fTwelg&merchant_order_no=159b1e55-4794-405c-b6d4-e767424c4cea&phone=13504508474&sign=D16A9A6DEFEFFB5FEF7369B4B908EDE5&sign_type=MD5&usdt_address=" "https://api.acepay.net/api/v2/daifu/request"`

&emsp;&emsp;功能说明：商户提交代付请求，采用HTTP POST交互。返回的结果，只能说明代付请求是否被接受了。通过代付查询接口，获取代付的当前状态。只有此应答中明确表示请求处理失败（status=2），才可以认为代付失败了。

**代付查询**

&emsp;&emsp;请求URL：https://api.acepay.net/api/v2/daifu/query

&emsp;&emsp;功能说明：商户提交查询请求，采用HTTP POST交互。同步返回代付的当前状态。通常情况下，代付的完成需要一段时间，请勿高频率查询

**代付回调**

&emsp;&emsp;回调地址：商户在支付平台配置的代付回调地址。

&emsp;&emsp;功能说明：支付平台向商户提交回调请求，请求商户的代付回调地址，请求的数据与代付查询接口返回的数据格式相同，采用HTTP POST交互，在支付平台代付完成（status=1）和代付失败（status=2）时向商户进行回调

### 2.2. 签名

&emsp;&emsp;目前仅支持MD5签名。

&emsp;&emsp;需要参与签名的参数：非空值参数，sign、sign_type两个参数不参与签名。

&emsp;&emsp;待签名串计算：将每个参数名和参数的值，组成&nbsp;参数名=参数值&nbsp;格式的字符串。按照参数名的次序将各个&nbsp;参数名=参数值&nbsp;字符串用&符号连接起来，得到字符串A。待签名串B=A&key=代付密钥。

&emsp;&emsp;签名计算： sign = md5(B)。

&emsp;&emsp;示例：

&emsp;&emsp;请求消息为： {'comment':'comment','phone':'13504508474','amount':'100','sign_type':'MD5','bank_code':'78','enc_data':'4QiS4CnswETuJPqK0NghxGs+wo4rhy3PAO597pXRfucR6TH93A8B05J7v4fTwelg', 'merchant_order_no': '159b1e55-4794-405c-b6d4-e767424c4cea', 'company_id': 'aaaccc', 'account_type': 'bank_card', 'usdt_address': '', 'currency': 'CNY', 'sign': ''}

&emsp;&emsp;密钥：9FHCGtSjBnXG7oePtEoaNqaKm4oNiMPm

&emsp;&emsp;待签名串B :

&emsp;&emsp;account_type=bank_card&amount=100&bank_code=78&comment=comment&company_id=aaaccc&currency=CNY&enc_data=4QiS4CnswETuJPqK0NghxGs+wo4rhy3PAO597pXRfucR6TH93A8B05J7v4fTwelg&merchant_order_no=159b1e55-4794-405c-b6d4-e767424c4cea&phone=13504508474&key=9FHCGtSjBnXG7oePtEoaNqaKm4oNiMPm

&emsp;&emsp;签名sign : D16A9A6DEFEFFB5FEF7369B4B908EDE5

### 2.3. 加密
&emsp;&emsp;敏感信息需要加密。目前，仅代付请求接口用到了加密。

&emsp;&emsp;加密算法：AES/ECB/PKCS5Padding。需要用base64对通过加密算法得到密文进行编码，编码的结果用于POST请求。

&emsp;&emsp;示例：

&emsp;&emsp;数据： 张飞|613100010002|31001019901205213

&emsp;&emsp;密钥： 9FHCGtSjBnXG7oePtEoaNqaKm4oNiMPm

&emsp;&emsp;加密结果为：4QiS4CnswETuJPqK0NghxGs+wo4rhy3PAO597pXRfucR6TH93A8B05J7v4fTwelg


----------


## 3. 请求代付
### 3.1. 请求消息

|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| company_id | 商户ID :支付平台提供给商户的编码。 | 32 |  否|
| merchant_order_no | 商户订单号。商户需要保证此订单号，在商户系统内是唯一的。 | 36 | 否 |
| currency | 币种，CNY表示代付人民币，USDT表示代付USDT，默认CNY。 | 10 | 是 |
| amount | 代付金额： 数字，必须两位小数点，无千位符。 | 10 | 否 |
| account_type | 代付账户类型。参见附录中的代付账户类型列表，这里使用的是列表中的代付账户类型代码列，默认值为bank_card。 | 20 | 是 |
| bank_code | 银行编码。参见附录中的银行编码列表，account_type为bank_card时必传。 | 5 | 是 |
| phone | 收款人手机号。 | 11 | 否 |
| usdt_address | USDT收款地址，USDT代付时必传。 | 11 | 是 |
| enc_data | 加密数据。内容：AES（姓名\|卡号（account_type为bank_card时这里是银行卡号，USDT代付时这里为空）\|身份证号）。AES加密的密钥为代付密钥。 | - | 否 |
| comment | 备注。目前没有具体的用途。 | 128 | 是 |
| sign_type | 签名类型。目前只能是MD5。 | 8 |否  |
| sign | 签名。 | 32 | 否 |

### 3.2. 应答消息

|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| code | 应答码。 200表示请求处理成功，非200表示请求处理过程中遇到了问题。<br>注意：不能用这个字段判断代付是否成功了。 | 8 |  否|
| merchant_order_no | 商户订单号。 | 36 | 否 |
| amount | 代付金额。 | 10 | 否 |
| error_message | 错误信息， 处理代付遇到问题时，会在此字段显示更多相关信息。 | 64 | 可 |
| status | 代付订单的当前状态：<br>1：代付成功<br>2：代付失败<br>4：代付处理中 <br>-2：代付订单不存在。 | 4  | 否 |
| sign_type | 签名算法。目前只能是MD5。 | 8 | 否 |
| sign | 签名。 | 32 | 否 |


----------


## 4. 代付查询
### 4.1. 请求消息

|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| company_id | 商户ID :支付平台提供给商户的编码。 | 32 |  否|
| merchant_order_no | 商户订单号。商户需要保证此订单号，在商户系统内是唯一的。 | 36 | 否 |
| sign_type | 签名算法。目前只能是MD5。 | 8 | 否 |
| sign | 签名。 | 32 | 否 |

### 4.2. 应答消息

|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| code | 应答码。 200表示请求处理成功，非200表示请求处理过程中遇到了问题。<br>注意：不能用这个字段判断代付是否成功了。 | 8 |  否|
| merchant_order_no | 商户订单号。 | 36 | 否 |
| amount | 代付金额。 | 10 | 否 |
| error_message | 错误信息， 处理代付遇到问题时，会在此字段显示更多相关信息。 | 64 | 可 |
| status | 代付订单的当前状态：<br>1：代付成功<br>2：代付失败<br>4：代付处理中 <br>-2：代付订单不存在。 | 4  | 否 |
| sign_type | 签名算法。目前只能是MD5。 | 8 | 否 |
| sign | 签名。 | 32 | 否 |

## 5. 代付回调
### 5.1. 请求消息（平台向商户提交的请求）

|  参数名称   | 参数说明  | 长度 | 可否为空 |
|  ----  | ----  | :----: | :----: |
| code | 应答码。 200表示请求处理成功，非200表示请求处理过程中遇到了问题。<br>注意：不能用这个字段判断代付是否成功了。 | 8 |  否|
| merchant_order_no | 商户订单号。 | 36 | 否 |
| amount | 代付金额。 | 10 | 否 |
| error_message | 错误信息， 处理代付遇到问题时，会在此字段显示更多相关信息。 | 64 | 可 |
| status | 代付订单的当前状态：<br>1：代付成功<br>2：代付失败<br>4：代付处理中 <br>-2：代付订单不存在。 | 4  | 否 |
| sign_type | 签名算法。目前只能是MD5。 | 8 | 否 |
| sign | 签名。 | 32 | 否 |

### 5.2. 应答消息
  需要向支付平台返回HTTP状态码：200

----------


## 6. 银行编码
| 银行名称  | 银行编码 | 银行缩写 |
| :----: | :----: | :----: |
| 工商银行 | 1 | ICBC |
| 招商银行 | 2 | CMBCHINA |
| 建设银行 | 3 | CCB |
|农业银行 | 4 | ABC |
|中国银行 | 5 | BOC |
|交通银行 | 6 | BOCO |
|民生银行 | 7 | CMBC |
| 中信银行 | 8 | ECITIC |
|上海浦东发展银行 | 9 | SPDB |
|中国邮政储汇 | 10 | POST |
| 光大银行 | 11 | CEB |
|广发银行 | 13 | CGB |
|兴业银行 | 15 |  CIB|
|北京银行 | 70 | BCCB |
|上海银行 | 71 | SHB |
|华夏银行 | 72 | HXBANK |
|宁波银行 | 73 | NBBANK |
|恒生银行 | 74 | HSB |
|恒丰银行 | 75 | EGB |
|渣打银行 | 76 | ZDYH |
|平安银行 | 77 | SZPA |
|长沙银行 | 78 | BCS |
|浙商银行 | CZ | CZ |
| 广东农村信用社 | 79 | GDRCU |
| 广州农村商业银行 | 80 | GRCB |
| 广西农村信用社 | 81 | GXRCU |
| 广西北部湾银行 | 82 | BGB |
| 广州银行 | 83 | GZCB |
| 上海农村商业银行 | 84 | SRCB |
| 渤海银行 | 85 | CBHB |
| 浙江省农村信用社 | 86 | ZJRCU |
| 青岛银行 | 87 | QDCCB |
| 北京农商银行 | 88 | BJRCB |
| 东莞农村商业银行 | 89 | DRCBANK |

## 7. 代付账户类型
| 代付账户类型代码  | 代付账户类型说明 | 应用代付类型 |
| :----: | :----: | :----: |
| bank_card | 银行卡 | CNY代付 |
| Alipay | 支付宝 | CNY代付 |
| Wechat | 微信 | CNY代付 |
| Erc20 | USDT-ERC20 | USDT代付 |
| Trc20 | USDT-TRC20 | USDT代付 |
| Omni | USDT-OMNI | USDT代付 |
